//
// Created by adria on 17/03/2020.
//

#include <stdio.h>
#include "MENU_VIEW.h"

#define BUFFER_SIZE 50


void MENU_VIEW_displayError(String* errMsg){
    printf("ERROR: %s", STRING_getString(errMsg));
}

void MENU_VIEW_displayMainMenu() {
    printf("1. Configure Car\n"
           "2. Race\n"
           "3. See Standings\n"
           "4. Save Season\n");
}


void MENU_VIEW_displayWelcomeMsg() {
    printf("Welcome to LS Racing !\n");
}

void MENU_VIEW_displaySpacing(int spacing) {
    int i;
    for(i = 0; i < spacing; i++) {
        printf("\n");
    }
}

String* queryConsoleStringInput(String *queryQuestion) {
    String* out = STRING_init();
    char buffer[BUFFER_SIZE] = {'\0'};
    while(buffer[0] != '\0' && buffer[0] != '\n') {
        printf("%s", STRING_getString(queryQuestion));
        fgets(buffer, BUFFER_SIZE, stdin);
    }
    STRING_set(out, buffer);
    return out;
}
