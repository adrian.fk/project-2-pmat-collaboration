//
// Created by adria on 17/03/2020.
//

#ifndef P2_ADRIAN_FALCH_MENU_PRESENTER_H
#define P2_ADRIAN_FALCH_MENU_PRESENTER_H

#include "../Utils/DynamicArray.h"
#include "../Utils/SimplefiedStrings.h"






/*********************************************************************************
Param: queryQuestions : Is an array of strings representing the different queries.
Return: Array : an array filled with Strings representing the different answers
                to the different queries.
 *******************************************************************************/
Array getStringArrInputs(Array queryQuestions[]);

String getStringInput(String queryQuestion);






#endif //P2_ADRIAN_FALCH_MENU_PRESENTER_H
