//
// Created by adria on 17/03/2020.
//

#ifndef P2_ADRIAN_FALCH_MENU_VIEW_H
#define P2_ADRIAN_FALCH_MENU_VIEW_H


#include "../Utils/SimplefiedStrings.h"

void MENU_VIEW_displayError(String* errMsg);

void MENU_VIEW_displayMainMenu();

void MENU_VIEW_displayWelcomeMsg();

void MENU_VIEW_displaySpacing(int spacing);

String* queryConsoleStringInput(String *queryQuestion);











#endif //P2_ADRIAN_FALCH_MENU_VIEW_H
