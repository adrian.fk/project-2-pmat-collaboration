//
// Created by adria on 20/03/2020.
//

#ifndef P2_ADRIAN_FALCH_FILESERRORHANDLER_H
#define P2_ADRIAN_FALCH_FILESERRORHANDLER_H


int FILES_ERROR_HANDLER_validateNumArgs(int numArgs, int validNumArgs);

int FILES_ERROR_HANDLER_validateFileContent(char *filename);

#endif //P2_ADRIAN_FALCH_FILESERRORHANDLER_H
