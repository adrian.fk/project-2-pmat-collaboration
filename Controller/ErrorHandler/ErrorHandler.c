//
// Created by adria on 20/03/2020.
//

#include <bits/types/FILE.h>
#include <sys/stat.h>
#include <stdio.h>
#include "ErrorHandler.h"
#include "../../Utils/Constants.h"


int FILES_ERROR_HANDLER_validateNumArgs(int numArgs, int validNumArgs) {
    return numArgs == validNumArgs;
}

int FILES_ERROR_HANDLER_validateFileContent(char *filename) {
    struct stat fileStat;
    stat(filename, &fileStat);

    if(fileStat.st_size !=0 ) {   //check if the file is not empty.
        return TRUE;
    }
    return FALSE;
}