//
// Created by adria on 03/03/2020.
//

#include <bits/types/FILE.h>
#include <stdio.h>
#include "BinFileLoader.h"

void BINFILE_load(char* _filePath, void* _dest, int _size) {
    FILE* fp = fopen(_filePath, "rb");
    if(!fp) {
        //Handle error
    }
    else {
        fread(_dest, (size_t)_size, 1,fp);
        fclose(fp);
    }
}


void BINFILE_loadAll(char* _filePath, Array _dest, void* _typeBuffer, int _size) {
    FILE* fp = fopen(_filePath, "rb");

    if(!fp) {
        //Handle error
    }
    else {
        while (fread(_typeBuffer, (size_t)_size, 1,fp))
            ARRAY_add(_dest, _typeBuffer);
        fclose(fp);
    }
}