//
// Created by adria on 03/03/2020.
//

#include "../../../Utils/DynamicArray.h"

#ifndef P2_ADRIAN_FALCH_BINFILELOADER_H
#define P2_ADRIAN_FALCH_BINFILELOADER_H

#endif //P2_ADRIAN_FALCH_BINFILELOADER_H

void BINFILE_load(char* _filePath, void* _dest, int _size);

void BINFILE_loadAll(char* _filePath, Array _dest, void* _typeBuffer, int _size);