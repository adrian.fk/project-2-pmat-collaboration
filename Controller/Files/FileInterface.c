//
// Created by adria on 21/03/2020.
//

#include "FileInterface.h"
#include "FileLoaders/TxtFileLoader.h"
#include "FileParsers/ComponentParser.h"
#include "FileParsers/GPsrParser.h"
#include "FileLoaders/BinFileLoader.h"

#define TEXT_FILE_PARTS 673
#define TEXT_FILE_GPS 525
#define BIN_FILE_BASE 684
#define BIN_FILE_RACERS 245


String* getTextFile(FileManager *fm, int searchIndicator);

int findChar(const char *charArr, char character, int startpoint);

int evaluateFileCondition(char character, int searchIndicator);

char* getBinFilepath(FileManager *fm, int searchIndicator);

String* getTextFile(FileManager *fm, int searchIndicator) {
    int i = 0;
    int categoryNameLocation;
    int found = FALSE;
    String* textFileContainer = STRING_init();

    while(!found) {
        if(FILEMANAGER_getFiletype(fm, i++) == FILETYPE_TEXT) {
            categoryNameLocation = 0;
            TEXTFILE_load(FILEMANAGER_getFilepath(fm, i), textFileContainer);
            categoryNameLocation = findChar(
                    STRING_getString(textFileContainer), '\n',
                    FIRST_ITEM
                    ) + 1;

            if(evaluateFileCondition(
                    STRING_getChar(textFileContainer, categoryNameLocation),
                    searchIndicator)) {
                found = TRUE;
            }
        }
    }
    return textFileContainer;
}

int evaluateFileCondition(char character, int searchIndicator) {
    int out = 0;
    int gpOut = FALSE;
    int partsOut = FALSE;

    switch(searchIndicator) {
        case TEXT_FILE_GPS:
            gpOut = TRUE;
            break;

        case TEXT_FILE_PARTS:
            partsOut = TRUE;
            break;

        default:break;
    }

    if((character >= 'a' && character <= 'z')
       || (character >= 'A' && character <= 'Z')) {
        out = partsOut;
    }
    else {
        out = gpOut;
    }

    return out;
}

int findChar(const char *charArr, char character, int startpoint) {
    int i = startpoint;
    while(charArr[i++] != character){}
    return i;
}

char* getBinFilepath(FileManager *fm, int searchIndicator) {
    int i;
    long maxSizeFound = 0;
    int racersFileTracker = 0;
    int baseFileTracker = 0;
    char* out = NULL;

    for(i = FIRST_ARG; i <= FILEMANAGER_getNumFiles(fm); i++) {
        if(FILEMANAGER_getFiletype(fm, i) == FILETYPE_BIN) {
            if(FILEMANAGER_getFileLength(fm, i) > maxSizeFound) {
                maxSizeFound = FILEMANAGER_getFileLength(fm, i);
                baseFileTracker = racersFileTracker;
                racersFileTracker = i;
            }
            else
                baseFileTracker = i;
        }
    }
    switch(searchIndicator) {
        case BIN_FILE_RACERS:
            out = FILEMANAGER_getFilepath(fm, racersFileTracker);
            break;

        case BIN_FILE_BASE:
            out = FILEMANAGER_getFilepath(fm, baseFileTracker);
            break;

        default:break;
    }
    return out;
}

Array FILE_getPartsFileArray(FileManager* fm) {
    Array out = ARRAY_constructor(sizeof(struct Component));
    //Find correct file
    String* partsFileString = getTextFile(fm, TEXT_FILE_PARTS);
    //Parse file
    PARSER_PARTS_parseToArray(out, partsFileString);

    return out;
}

Array FILE_getGpFileArray(FileManager* fm) {
    Array out = ARRAY_constructor(sizeof(struct AllGrandPrix));
    //Find correct file
    String* GPsFileString = getTextFile(fm, TEXT_FILE_GPS);
    //Parse file
    PARSER_GPS_parseToArray(out, GPsFileString);

    STRING_destroy(GPsFileString);
    return out;
}

Array FILE_getRacersAsArray(FileManager* fm) {
    Array out = ARRAY_constructor(sizeof(struct Racer));
    struct Racer racerBuffer;

    char* racersFileLocation = getBinFilepath(fm, BIN_FILE_RACERS);

    BINFILE_loadAll(racersFileLocation, out, &racerBuffer, sizeof(struct Racer));

    return out;
}

struct GameBase FILE_getGameBase(FileManager* fm) {
    struct GameBase gameBaseBuffer = {0,0,0,0};

    char* baseFileLocation = getBinFilepath(fm, BIN_FILE_BASE);

    BINFILE_load(baseFileLocation, &gameBaseBuffer, sizeof(struct GameBase));

    return gameBaseBuffer;
}

