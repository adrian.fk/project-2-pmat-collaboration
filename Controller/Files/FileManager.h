//
// Created by adria on 20/03/2020.
//

#ifndef P2_ADRIAN_FALCH_FILESMANAGER_H
#define P2_ADRIAN_FALCH_FILESMANAGER_H

#include "../../Utils/Constants.h"


#define FILEMANAGER_VALID_NUM_ARGS 4

//File types
#define FILETYPE_BIN 11
#define FILETYPE_TEXT 12

#define FILEMANAGER_ERROR_NO_ERROR 0
#define FILEMANAGER_ERROR_INSUFFICIENT_ARGS 30
#define FILEMANAGER_ERROR_PROCESS_FILES 31
#define FILEMANAGER_ERROR_FILE_EMPTY 32
#define FILEMANAGER_ERROR_MALLOC 40


typedef struct fileManager FileManager;


//We have to check for filetype in the constructor
FileManager* FILEMANAGER_init(int numArgs, char **args);

int FILEMANAGER_validateAttachedFiles(FileManager *fm);

int FILEMANAGER_getError(FileManager* fm);

int FILEMANAGER_getNumFiles(FileManager* fm);

int FILEMANAGER_getFiletype(FileManager* fm, int targetIndex);

void FILEMANAGER_close(FileManager* fm);

void FILEMANAGER_loadFiles(FileManager* fm);

long FILEMANAGER_getFileLength(FileManager* fm, int targetIndex);

char* FILEMANAGER_getFilepath(FileManager* fm, int targetIndex);



#endif //P2_ADRIAN_FALCH_FILESMANAGER_H
