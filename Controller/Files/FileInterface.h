//
// Created by adria on 21/03/2020.
//

#ifndef P2_ADRIAN_FALCH_FILEINTERFACE_H
#define P2_ADRIAN_FALCH_FILEINTERFACE_H


#include "../../Utils/Constants.h"
#include "../../Utils/SimplefiedStrings.h"
#include "../../Utils/DynamicArray.h"
#include "FileManager.h"
#include "../../Model/Component.h"
#include "../../Model/AllGrandPrix.h"
#include "../../Model/Racer.h"
#include "../../Model/GameBase.h"


Array FILE_getPartsFileArray(FileManager* fm);

Array FILE_getGpFileArray(FileManager* fm);

Array FILE_getRacersAsArray(FileManager* fm);

struct GameBase FILE_getGameBase(FileManager* fm);


#endif //P2_ADRIAN_FALCH_FILEINTERFACE_H
