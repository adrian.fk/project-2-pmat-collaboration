//
// Created by adria on 20/03/2020.
//

#include <stdlib.h>
#include <bits/types/FILE.h>
#include <stdio.h>
#include "FileManager.h"
#include "../../Utils/SimplefiedStrings.h"




#define FILE_TYPE_SEPERATOR '.'
#define FILETYPE_BIN_EXT ".bin"
#define FILETYPE_TEXT_EXT ".txt"


struct fileManager {
    int error;
    int numFiles;
    char** filepath;
    int* fileType;
    long* filesize;
};


void validateFiles(FileManager *fm, char **filepath);

void validateFileContentExistence(FileManager *fm, int index);

void evaluateFileType(FileManager* fm, char* filepath, int argNumber) {
    int i;
    int fileTypeSpecifierFound = 0;
    String* typeSpecifier = STRING_init();

    for(i = (int)strlen(filepath) - 1; i > 0; i--) {
        if(filepath[i] == FILE_TYPE_SEPERATOR) {
            fileTypeSpecifierFound = i;
        }
    }
    if(fileTypeSpecifierFound) {
        for (i = fileTypeSpecifierFound; i < strlen(filepath); i++) {
            STRING_appendChar(typeSpecifier, filepath[i]);
        }
        if (strcmp(STRING_getString(typeSpecifier), FILETYPE_BIN_EXT) == 0) {
            fm->fileType[argNumber] = FILETYPE_BIN;
        }
        if (strcmp(STRING_getString(typeSpecifier), FILETYPE_TEXT_EXT) == 0) {
            fm->fileType[argNumber] = FILETYPE_TEXT;
        }
    }

    STRING_destroy(typeSpecifier);
}

FileManager* FILEMANAGER_init(int numArgs, char **args) {
    int numFileArguments = numArgs - 1;
    FileManager *fm = (FileManager *) malloc(sizeof(struct fileManager));
    if(fm) {
        fm->fileType = (int*) malloc(sizeof(int) * numFileArguments);
        if(fm->fileType) {
            fm->filesize = (long*) malloc(sizeof(long) * numFileArguments);
            if(fm->filesize) {
                if (numFileArguments == FILEMANAGER_VALID_NUM_ARGS) {
                    fm->error = FILEMANAGER_ERROR_NO_ERROR;
                    fm->filepath = args;
                    fm->numFiles = numFileArguments;
                }
                else
                    fm->error = FILEMANAGER_ERROR_INSUFFICIENT_ARGS;
            }
            else
                fm->error = FILEMANAGER_ERROR_MALLOC;
        }
        else
            fm->error = FILEMANAGER_ERROR_MALLOC;
    }
    return fm;
}

void validateFileContentExistence(FileManager *fm, int index) {
    String* testStr = STRING_init();
    STRING_set(testStr, fm->filepath[index]);
    FILE* fp = fopen(fm->filepath[index], "r");
    if(fp) {
        fseek(fp, 0, SEEK_END);
        fm->filesize[index] = ftell(fp);
        if(fm->filesize[index] == 0) {
            fm->error = FILEMANAGER_ERROR_FILE_EMPTY;
        }
    }
    else {
        fm->error = FILEMANAGER_ERROR_PROCESS_FILES;
    }
}

int FILEMANAGER_validateAttachedFiles(FileManager *fm) {
    int out = FILEMANAGER_ERROR_NO_ERROR;

    for(int i = FIRST_ARG; i <= fm->numFiles; i++) {
        validateFileContentExistence(fm, i);
        evaluateFileType(fm, fm->filepath[i], i);
    }
    if(fm->error) {
        out = fm->error;
    }
    return out;
}


int FILEMANAGER_getError(FileManager* fm) {
    return fm->error;
}

void FILEMANAGER_close(FileManager* fm) {
    free(fm->fileType);
    free(fm->filesize);
    free(fm);
}

int FILEMANAGER_getFiletype(FileManager* fm, int targetIndex) {
    int out = 0;
    if(fm->fileType[targetIndex]) {
        out = fm->fileType[targetIndex];
    }
    return out;
}

long FILEMANAGER_getFileLength(FileManager* fm, int targetIndex) {
    long out = 0;
    if(fm->filesize[targetIndex]) {
        out = fm->filesize[targetIndex];
    }
    return out;
}

char* FILEMANAGER_getFilepath(FileManager* fm, int targetIndex) {
    return fm->filepath[targetIndex];
}

int FILEMANAGER_getNumFiles(FileManager* fm) {
    return fm->numFiles;
}



