#include <stdio.h>

#include "Utils/SortedArray.h"
#include "Utils/SimplefiedStrings.h"
#include "Model/GameBase.h"
#include "Model/Racer.h"
#include "Controller/ErrorHandler/ErrorHandler.h"
#include "Controller/Files/FileLoaders/BinFileLoader.h"
#include "Controller/Files/FileLoaders/TxtFileLoader.h"
#include "Controller/Files/FileManager.h"
#include "Controller/Files/FileInterface.h"


int main(int argc, char *argv[]) {


    Array racers = ARRAY_constructor(sizeof(struct Racer));
    struct Racer racer1;

    BINFILE_loadAll("fitxerCorredors.bin", racers, &racer1, sizeof(struct Racer));
    BINFILE_load("fitxerCorredors.bin", &racer1, sizeof(struct Racer));

    struct Racer* printable_racer = ARRAY_get(racers, 1);

    printf("%s", RACER_getName(printable_racer));

    struct GameBase gameBase1;

    BINFILE_load("fitxerBase.bin", &gameBase1, sizeof(struct GameBase));

    String* testString = STRING_init();
    TEXTFILE_load("fitxerPeces.txt", testString);

    FileManager* fm = FILEMANAGER_init(argc, argv);
    FILEMANAGER_validateAttachedFiles(fm);


    struct GameBase gameBase = FILE_getGameBase(fm);
    int i = 2+2;

    return 0;
}