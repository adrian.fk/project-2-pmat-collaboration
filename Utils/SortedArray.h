#ifndef P2_ADRIAN_FALCH_SORTEDARRAY_H
#define P2_ADRIAN_FALCH_SORTEDARRAY_H

#endif //P2_ADRIAN_FALCH_SORTEDARRAY_H

#include "DynamicArray.h"

#define ASCENDING 1
#define DESCENDING 2

typedef struct sortedArray* SortedArray;

SortedArray SORTEDARRAY_init(size_t dataSize, int _sortDirection);

void SORTEDARRAY_sort(SortedArray sortedArray);

int SORTEDARRAY_getError(SortedArray arr);

void* SORTEDARRAY_get(SortedArray arrObj, int index);

float SORTEDARRAY_getSortable(SortedArray arrObj, int index);

void SORTEDARRAY_set(SortedArray arrObj, int index, void* input);

void SORTEDARRAY_add(SortedArray sortedArray, void* input, float sortable);

int SORTEDARRAY_getLength(SortedArray arrObj);

void SORTEDARRAY_destroy(SortedArray sortedArray);
