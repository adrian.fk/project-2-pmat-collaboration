//
// Created by adria on 03/03/2020.
//

#include <stdlib.h>
#include <stdio.h>
#include "SimplefiedStrings.h"

#define INPUT_BUFFER_SIZE 100

struct string {
    int length;
    char* string;
};

void STRING_appendChar(String* string, char input) {
    string->length += 1;
    string->string = (char*) realloc(string->string, string->length*sizeof(char));
    if(string->string) {
        string->string[string->length - 2] = input;
        string->string[string->length - 1] = '\0';
    }
}

void STRING_append(String* string, char input[]) {
    int inputLength = (int) strlen(input);
    size_t totalNewLength = (size_t) string->length + inputLength;
    string->string = (char*) realloc(string->string, totalNewLength);
    if(string->string) {
        for (int i = 0; i < inputLength; i++) {
            STRING_appendChar(string, input[i]);
        }
    }
    else {
        //handle error
    }
}


void STRING_set(String* string, char input[]) {
    int i;
    int length = (int) strlen(input);

    string->string = (char*) realloc(string->string, length * sizeof(char) + 1);
    if(string->string) {
        string->length = length + 1;
        for (i = 0; i < length; i++) {
            string->string[i] = input[i];
        }
        string->string[i] = '\0';
    }
    else {
        //handle error
    }
}

char* STRING_getString(String* strObj) {
    return strObj->string;
}

size_t STRING_getSizeOf() {
    return sizeof(String);
}

String* STRING_init() {
    String *out = (String*)malloc(sizeof(String));
    out->string = (char*) malloc(sizeof(char));
    out->length = 1;
    out->string[0] = '\0';
    return out;
}

void STRING_getInput(String* _dest) {
    char buffer[INPUT_BUFFER_SIZE] = {'\0'};
    fgets(buffer, INPUT_BUFFER_SIZE, stdin);
    STRING_set(_dest, buffer);
}

char STRING_getChar(String* strObj, int index) {
    char out = '\0';
    if(index < strObj->length) {
        out = strObj->string[index];
    }
    return out;
}

void STRING_destroy(String* _dest) {
    free(_dest->string);
    free(_dest);
}
