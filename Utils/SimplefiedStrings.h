//
// Created by adria on 03/03/2020.
//

#ifndef P2_ADRIAN_FALCH_SIMPLEFIEDSTRINGS_H
#define P2_ADRIAN_FALCH_SIMPLEFIEDSTRINGS_H

#endif //P2_ADRIAN_FALCH_SIMPLEFIEDSTRINGS_H

#include <string.h>

typedef struct string String;

void STRING_set(String* string, char input[]);

void STRING_append(String* string, char input[]);

void STRING_appendChar(String* string, char input);

char* STRING_getString(String* strObj);

char STRING_getChar(String* strObj, int index);

String* STRING_init();

void STRING_getInput(String* _dest);

void STRING_destroy(String* _dest);

size_t STRING_getSizeOf();


