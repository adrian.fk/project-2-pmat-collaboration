#include "SortedArray.h"

#define convertIndex(lgth) lgth - 1

struct sortedArray {
    Array arrObj;
    int sortDirection;
};

void processSort(SortedArray sortedArray);

int sortStatementFound(SortedArray sortedArray, int i);


SortedArray SORTEDARRAY_init(size_t dataSize, int _sortDirection) {
    SortedArray out = malloc(sizeof(struct sortedArray));
    out->arrObj = ARRAY_constructor(dataSize);
    out->sortDirection = _sortDirection;
    return out;
}


int SORTEDARRAY_getError(SortedArray arr) {
    return ARRAY_getError(arr->arrObj);
}

void* SORTEDARRAY_get(SortedArray arrObj, int index) {
    return ARRAY_get(arrObj->arrObj, index);
}

float SORTEDARRAY_getSortable(SortedArray arrObj, int index) {
    return ARRAY_getSortable(arrObj->arrObj, index);
}

void SORTEDARRAY_set(SortedArray arrObj, int index, void* input) {
    ARRAY_set(arrObj->arrObj, index, input);
    processSort(arrObj);
}

int sortStatementFound(SortedArray sortedArray, int i) {
    if (ASCENDING == sortedArray->sortDirection) {
        return (ARRAY_getSortable(sortedArray->arrObj, i) > ARRAY_getSortable(sortedArray->arrObj, i + 1));
    }
    return (ARRAY_getSortable(sortedArray->arrObj, i) < ARRAY_getSortable(sortedArray->arrObj, i + 1));
}

void processSort(SortedArray sortedArray) {
    int i, j;
    int arrayLength = ARRAY_getLength(sortedArray->arrObj);
    for (i = 0; i < arrayLength-1; i++) {
        for ( j = 0; j < arrayLength - i - 1; j++) {
            if(sortStatementFound(sortedArray, j))
                ARRAY_swap(sortedArray->arrObj, j, j+1);
        }
    }
}


void SORTEDARRAY_add(SortedArray sortedArray, void* input, float sortable) {
    ARRAY_add(sortedArray->arrObj, input);

    ARRAY_setSortable(
            sortedArray->arrObj,
            convertIndex(ARRAY_getLength(sortedArray->arrObj)),
            sortable
            );

    if(ARRAY_getError(sortedArray->arrObj)) {
        //TODO Handle error

        ARRAY_setErrorHandled(sortedArray->arrObj);
    }
}

void SORTEDARRAY_sort(SortedArray sortedArray) {
    if(SORTEDARRAY_getLength(sortedArray) > 1)
        processSort(sortedArray);
}

int SORTEDARRAY_getLength(SortedArray arrObj) {
    return ARRAY_getLength(arrObj->arrObj);
}

void SORTEDARRAY_destroy(SortedArray sortedArray) {
    ARRAY_destroy(sortedArray->arrObj);
    free(sortedArray->arrObj);
    sortedArray->arrObj = NULL;
}
