//
// Created by adria on 17/03/2020.
//

#ifndef P2_ADRIAN_FALCH_SAVABLERACERS_H
#define P2_ADRIAN_FALCH_SAVABLERACERS_H

#include "../Utils/SimplefiedStrings.h"

#define CHAR_ARR_SIZE 25


struct Racer {
    char name[CHAR_ARR_SIZE];
    int driverNumber;
    char team[CHAR_ARR_SIZE];
    int speed;
    int acceleration;
    int consumption;
    int reliability;
    int reflexes;
    int physicalCondition;
    int temperament;
    int tireManagement;
};


struct Racer* RACER_init();

char* RACER_getName(struct Racer* savableRacer);

int RACER_getDriverNumber(struct Racer* savableRacer);

char* RACER_getTeam(struct Racer* savableRacer);

int RACER_getSpeed(struct Racer* savableRacer);

int RACER_getAcceleration(struct Racer* savableRacer);

int RACER_getConsumption(struct Racer* savableRacer);

int RACER_getReliability(struct Racer* savableRacer);

int RACER_getReflexes(struct Racer* savableRacer);

int RACER_getPhysicalCondition(struct Racer* savableRacer);

int RACER_getTemperament(struct Racer* savableRacer);

int RACER_getTireManagement(struct Racer* savableRacer);

void RACER_destroy(struct Racer* racer);


#endif //P2_ADRIAN_FALCH_SAVABLERACERS_H
