//
// Created by adria on 17/03/2020.
//

#ifndef P2_ADRIAN_FALCH_GAMEBASE_H
#define P2_ADRIAN_FALCH_GAMEBASE_H

struct GameBase {
    int speed;
    int acceleration;
    int consumption;
    int releability;
};


#endif //P2_ADRIAN_FALCH_GAMEBASE_H
